DROP TABLE IF EXISTS departments CASCADE;
CREATE TABLE departments (
    dept_no VARCHAR NOT NULL,
    dept_name VARCHAR NOT NULL,
    PRIMARY KEY (dept_no)
);

DROP TABLE IF EXISTS titles CASCADE;
CREATE TABLE titles (
	emp_no INT NOT NULL,
	title VARCHAR NOT NULL,
	from_date DATE,
	to_date DATE
);

DROP TABLE IF EXISTS employees CASCADE;
CREATE TABLE employees (
    emp_no INT NOT NULL,
    birth_date DATE NOT NULL,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    gender VARCHAR NOT NULL,
    hire_date DATE NOT NULL,
    PRIMARY KEY (emp_no)
);

DROP TABLE IF EXISTS salaries CASCADE;
CREATE TABLE salaries (
    emp_no INT NOT NULL,
    salary FLOAT NOT NULL,
	from_date DATE,
	to_date DATE,
    FOREIGN KEY (emp_no) REFERENCES employees (emp_no)
);

DROP TABLE IF EXISTS dept_emp CASCADE;
CREATE TABLE dept_emp (
    emp_no INT NOT NULL,
    dept_no VARCHAR NOT NULL,
	from_date DATE,
	to_date DATE,
    FOREIGN KEY (emp_no) REFERENCES employees (emp_no),
    FOREIGN KEY (dept_no) REFERENCES departments (dept_no),
    PRIMARY KEY (emp_no,dept_no)
);

DROP TABLE IF EXISTS dept_manager CASCADE;
CREATE TABLE dept_manager (
	emp_no INT NOT NULL,
    dept_no VARCHAR NOT NULL,
	from_date DATE,
	to_date DATE,
    FOREIGN KEY (emp_no) REFERENCES employees (emp_no),
    FOREIGN KEY (dept_no) REFERENCES departments (dept_no)
);

COPY departments(dept_no,dept_name) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/departments.csv' DELIMITER ',' CSV HEADER;

COPY titles(emp_no,title,from_date,to_date) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/titles.csv' DELIMITER ',' CSV HEADER;

COPY employees(emp_no,birth_date,first_name,last_name,gender,hire_date) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/employees.csv' DELIMITER ',' CSV HEADER;

COPY salaries(emp_no,salary,from_date,to_date) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/salaries.csv' DELIMITER ',' CSV HEADER;

COPY dept_emp(emp_no,dept_no,from_date,to_date) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/dept_emp.csv' DELIMITER ',' CSV HEADER;

COPY dept_manager(dept_no,emp_no,from_date,to_date) 
FROM '/home/mike/projects/Bootcamp/sql-challenge/EmployeeSQL/Resources/dept_manager.csv' DELIMITER ',' CSV HEADER;











